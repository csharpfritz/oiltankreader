﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.IO;

namespace OilTankReader
{
	// To learn more about Microsoft Azure WebJobs SDK, please see https://go.microsoft.com/fwlink/?LinkID=320976
	class Program
	{
		// Please set the following connection strings in app.config for this WebJob to run:
		// AzureWebJobsDashboard and AzureWebJobsStorage
		static void Main()
		{

#if DEBUG
			using (var testImage = new StreamReader(@"..\..\..\..\testImages\image001.png"))
			{
				var sb = new StringBuilder();
				var writer = new StringWriter(sb);
				Functions.ProcessNewImage(testImage.BaseStream, "Test Image", writer);
				Console.WriteLine(sb);
				Console.ReadLine();
			}
#endif

			var config = new JobHostConfiguration();

			if (config.IsDevelopment)
			{
				config.UseDevelopmentSettings();
			}

			var host = new JobHost();
			// The following code ensures that the WebJob will be running continuously
			host.RunAndBlock();
		}
	}
}
