﻿using Leadtools;
using Leadtools.Codecs;
using Leadtools.Forms;
using Leadtools.Forms.Ocr;
using Leadtools.ImageProcessing.Color;
using Leadtools.ImageProcessing.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ImageProcessor
{
	public class Handler
	{

		private RasterCodecs _Codecs = new RasterCodecs();
		private static readonly LogicalRectangle GaugeWindow = new LogicalRectangle(71, 66, 61, 145, LogicalUnit.Pixel);

		static Handler()
		{

			Leadtools.RasterSupport.SetLicense("eval-license-files.lic", File.ReadAllText("eval-license-files.lic.key"));

		}

		public static List<string> Execute(Stream inputStream)
		{

			var thisHandler = new Handler();

			var processedImage = thisHandler.PreProcessImage(inputStream);
			return thisHandler.ReadGauge(processedImage);

		}

		private RasterImage PreProcessImage(Stream inputStream)
		{

			if (_Codecs == null) _Codecs = new RasterCodecs();

			// Load the image
			var img = _Codecs.Load(inputStream);

			// Crop the image
			var croppedImg = new AutoCropCommand();
			croppedImg.Run(img);

			// enhance the colors in the image to compensate for light
			var autoColor = new AutoColorLevelCommand();
			autoColor.Run(img);

			// Convert to black and white 
			var autoBin = new AutoBinarizeCommand();
			autoBin.Run(img);

			return img;

		}

		private List<string> ReadGauge(RasterImage img)
		{

			var foundText = "";

			using (var engine = OcrEngineManager.CreateEngine(OcrEngineType.Advantage, false))
			{

				engine.Startup(_Codecs, null, null, null);
				var page = engine.CreatePage(img, OcrImageSharingMode.None);

				var numberZone = new OcrZone();
				numberZone.Bounds = GaugeWindow;
				numberZone.CharacterFilters = OcrZoneCharacterFilters.Digit;
				page.Zones.Add(numberZone);

				page.Recognize(null);
				try
				{
					foundText = page.GetText(0);

					foundText = CalculateExactVolume(page.GetText(0), page.GetRecognizedCharacters()[0]);
				}
				catch (RasterException ex)
				{
					// no data found
					foundText = "<< no data found >>";
				}
			}

			return foundText.Split('\n').ToList();

		}

		private string CalculateExactVolume(string foundText, IOcrZoneCharacters ocrCharacters)
		{

			// Identify where the lowest number is located in the identified text
			var charCount = foundText.IndexOf('\n') - 1;

			// Grab the raw OCR Number for inspection
			var foundCharacter = ocrCharacters.Skip(charCount).Take(1).First();

			var digitHeight = foundCharacter.Bounds.Height;
			var indicatorPosition = GaugeWindow.Y + GaugeWindow.Height / 2;		// indicator arrow is in the middle
			var digitBottom = foundCharacter.Bounds.Bottom;
			var distanceFromBottom = digitBottom - indicatorPosition;

			var adjustment = ((digitHeight - distanceFromBottom)/digitHeight)*10 - 5;

			var txt = double.Parse(foundText.Split('\n')[1].Trim() + "0") + adjustment;

			return Math.Round(txt, 1).ToString();

		}
	}
}
