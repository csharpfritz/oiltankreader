﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using System.Drawing;
using ImageProcessor;

namespace OilTankReader
{
	public static class Functions
	{
	
		// This function will get triggered/executed when a new message is written 
		// on an Azure Queue called queue.
		public static void ProcessNewImage([BlobTrigger("oiltankphoto/{name}")] Stream inputStream, string blobTrigger, TextWriter log)
		{

			var gaugeReadings = Handler.Execute(inputStream);

			log.WriteLine($"Processed image {blobTrigger}");
			foreach (var reading in gaugeReadings)
			{
				log.WriteLine($"Found value: {reading}");
			}
			log.WriteLine("Completed processing\n\n");

		}


	}
}
