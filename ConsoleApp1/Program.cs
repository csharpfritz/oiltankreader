﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
	class Program
	{
		static void Main(string[] args)
		{

			using (var testImage = new StreamReader(@"..\..\..\..\testImages\image001.png"))
			{
				var sb = new StringBuilder();
				var writer = new StringWriter(sb);
				var outList = ImageProcessor.Handler.Execute(testImage.BaseStream);
				Console.WriteLine(string.Join("\n",outList.ToArray()));
				Console.ReadLine();
			}

		}
	}
}
